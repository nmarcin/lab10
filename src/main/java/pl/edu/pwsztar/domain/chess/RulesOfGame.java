package pl.edu.pwsztar.domain.chess;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.swing.*;

public interface RulesOfGame {

    boolean isCorrectMove(int xStart, int yStart, int xEnd, int yEnd);

    @Component
    @Qualifier("Bishop")
    class Bishop implements RulesOfGame {

        @Override
        public boolean isCorrectMove(int xStart, int yStart, int xEnd, int yEnd) {
            if(xStart == xEnd && yStart == yEnd) {
                return false;
            }
            return Math.abs(xEnd - xStart) == Math.abs(yEnd - yStart);
        }
    }

    @Component
    @Qualifier("Knight")
    class Knight implements RulesOfGame {

        @Override
        public boolean isCorrectMove(int xStart, int yStart, int xEnd, int yEnd) {
            if (xStart == xEnd && yStart == yEnd) {
                return false;
            }
            boolean xMove = ((Math.abs(xEnd - xStart) == 2 && Math.abs(yEnd - yStart) == 1));
            boolean yMove = ((Math.abs(xEnd - xStart) == 1 && Math.abs(yEnd - yStart) == 2));
            return xMove ^ yMove;

        }
    }

    @Component
    @Qualifier("King")
    class King implements RulesOfGame {

        @Override
        public boolean isCorrectMove(int xStart, int yStart, int xEnd, int yEnd) {
            if (xStart == xEnd && yStart == yEnd) {
                return false;
            }
            boolean Move = Math.abs(xEnd - xStart) < 2 && Math.abs(yEnd - yStart) < 2;
            return Move;
        }
    }

    @Component
    @Qualifier("Queen")
    class Queen implements RulesOfGame {

        @Override
        public boolean isCorrectMove(int xStart, int yStart, int xEnd, int yEnd) {
            if (xStart == xEnd && yStart == yEnd) {
                return false;
            }
            boolean Move = Math.abs(xEnd - xStart) == Math.abs(yEnd - yStart);
            boolean sMove = (xEnd == xStart) ^ (yEnd == yStart);
            return Move ^ sMove;
        }
    }

    @Component
    @Qualifier("Rook")
    class Rook implements RulesOfGame {

        @Override
        public boolean isCorrectMove(int xStart, int yStart, int xEnd, int yEnd) {
            if (xStart == xEnd && yStart == yEnd) {
                return false;
            }
            int xMoveLength = Math.abs(xStart - xEnd);
            int yMoveLength = Math.abs(yStart - yEnd);
            return xMoveLength == 0 ^ yMoveLength == 0;
        }
    }

    @Component
    @Qualifier("Pawn")
    class Pawn implements RulesOfGame {

        @Override
        public boolean isCorrectMove(int xStart, int yStart, int xEnd, int yEnd) {
            if (xStart == xEnd && yStart == yEnd) {
                return false;
            }
            boolean xMove = ((yEnd == yStart+1) && (xEnd == xStart));
            boolean yMove = ((yEnd == yStart+2) && (xEnd == xStart));
            return xMove ^ yMove;
        }
    }
}
