package pl.edu.pwsztar.service.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import pl.edu.pwsztar.domain.chess.RulesOfGame;
import pl.edu.pwsztar.domain.dto.FigureMoveDto;
import pl.edu.pwsztar.service.ChessService;

@Service
public class ChessServiceImpl implements ChessService {

    private RulesOfGame bishop;
    private RulesOfGame knight;
    private RulesOfGame king;
    private RulesOfGame queen;
    private RulesOfGame rook;
    private RulesOfGame pawn;

    @Autowired
    public ChessServiceImpl(@Qualifier("Bishop") RulesOfGame bishop,
                            @Qualifier("Knight") RulesOfGame knight,
                            @Qualifier("King") RulesOfGame king,
                            @Qualifier("Queen") RulesOfGame queen,
                            @Qualifier("Rook") RulesOfGame rook,
                            @Qualifier("Pawn") RulesOfGame pawn) {

        this.bishop = bishop;
        this.knight = knight;
        this.king = king;
        this.queen = queen;
        this.rook = rook;
        this.pawn = pawn;
    }


    @Override
    public boolean isCorrectMove(FigureMoveDto figureMoveDto) {
        String[] startPositions = figureMoveDto.getStart().split("_");
        int xStart = Integer.parseInt(startPositions[0]);
        int yStart = Integer.parseInt(startPositions[1]);

        String[] destinationPositions = figureMoveDto.getDestination().split("_");
        int xDestination = Integer.parseInt(destinationPositions[0]);
        int yDestination = Integer.parseInt(destinationPositions[1]);

        switch (figureMoveDto.getType()) {
            case BISHOP:
                return bishop.isCorrectMove(xStart, yStart, xDestination, yDestination);
            case KNIGHT:
                return knight.isCorrectMove(xStart, yStart, xDestination, yDestination);
            case KING:
                return king.isCorrectMove(xStart, yStart, xDestination, yDestination);
            case QUEEN:
                return queen.isCorrectMove(xStart, yStart, xDestination, yDestination);
            case ROCK:
                return rook.isCorrectMove(xStart, yStart, xDestination, yDestination);
            case PAWN:
                return pawn.isCorrectMove(xStart, yStart, xDestination, yDestination);
            default:
                return false;
        }
    }
}